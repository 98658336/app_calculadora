


package com.example.app_calculadora;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    private TextView tvUsuario;
    private EditText txtUsuario;
    private TextView tvContraseña;
    private EditText txtContraseña;
    private Button btnIngresar;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Referencias a los elementos de la interfaz
        tvUsuario = findViewById(R.id.tvUsuario);
        txtUsuario = findViewById(R.id.txtUsuario);
        tvContraseña = findViewById(R.id.tvContraseña);
        txtContraseña = findViewById(R.id.txtContraseña);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnSalir = findViewById(R.id.btnSalir);

        // Acciones de los botones
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String usuario = txtUsuario.getText().toString();
                String contraseña = txtContraseña.getText().toString();


                boolean usuarioValido = usuario.equals(USUARIO);
                boolean contraseñaValida = contraseña.equals(CONTRASEÑA);

                if (usuarioValido && contraseñaValida) {
                    Intent intent = new Intent(MainActivity.this, NuevaActividad.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                }
            }
        });




        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private static final String USUARIO = "admin";
    private static final String CONTRASEÑA = "123456";

    private boolean validarUsuario(String usuario) {
        return usuario.equals(USUARIO);
    }
}