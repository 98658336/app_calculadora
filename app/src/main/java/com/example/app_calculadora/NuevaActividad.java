package com.example.app_calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class NuevaActividad extends AppCompatActivity {


    private EditText txtNumero1, txtNumero2, txtResultado;
    private Button btnSuma, btnResta, btnMultiplicacion, btnDivision, btnLimpiar, btnRegresar;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_actividad);

        txtNumero1 = findViewById(R.id.txtNumero1);
        txtNumero2 = findViewById(R.id.txtNumero2);
        txtResultado = findViewById(R.id.txtResultado);
        btnSuma = findViewById(R.id.btnSuma);
        btnResta = findViewById(R.id.btnResta);
        btnMultiplicacion = findViewById(R.id.btnMultiplicacion);
        btnDivision = findViewById(R.id.btnDivision);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numero1 = Integer.parseInt(txtNumero1.getText().toString());
                int numero2 = Integer.parseInt(txtNumero2.getText().toString());
                int resultado = numero1 + numero2;
                txtResultado.setText(String.valueOf(resultado));
            }
        });

        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numero1 = Integer.parseInt(txtNumero1.getText().toString());
                int numero2 = Integer.parseInt(txtNumero2.getText().toString());
                int resultado = numero1 - numero2;
                txtResultado.setText(String.valueOf(resultado));
            }
        });

        btnMultiplicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numero1 = Integer.parseInt(txtNumero1.getText().toString());
                int numero2 = Integer.parseInt(txtNumero2.getText().toString());
                int resultado = numero1 * numero2;
                txtResultado.setText(String.valueOf(resultado));
            }
        });

        btnDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numero1 = Integer.parseInt(txtNumero1.getText().toString());
                int numero2 = Integer.parseInt(txtNumero2.getText().toString());
                int resultado = numero1 / numero2;
                txtResultado.setText(String.valueOf(resultado));
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNumero1.setText("");
                txtNumero2.setText("");
                txtResultado.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
